#!/usr/bin/env python

#this is a drop in "example" for the fadecandy addressable led driver.
#get started here https://github.com/scanlime/fadecandy 


# Light each LED in sequence, and repeat.
import opc, time, math, datetime, colorsys, threading
import random 

# want screen, but this has blend
# https://pypi.org/project/colorutils/
#from colorutils import Color

numLEDs = (228)
numOfStars = 10

client = opc.Client('localhost:7890')
sum_of_leds = numLEDs

epoch = time.time()
print("Start: " + str(epoch) + " ")
 
class star:

    center = 100
    hue = 0
    saturation = 1
    brightness = 255
    duration = 100
    pix_width = 10
    fade_shape = 0 # enum bell, triangle, square, sine, ?
    fade_time = 0 # enum bell, triangle, square, sine, ?
    start_time = time.time()
    star_pixels = [ (0,0,0) ] * (pix_width)

    def __init__(self, center, hue, saturation, brightness, duration, pix_width, fade_shape, fade_time, start_time):
        self.center = center
        self.center_start = self.center
        self.center_distance = 0
        self.hue = hue
        self.saturation = saturation
        self.brightness = brightness
        self.duration = duration
        self.pix_width = pix_width
        self.fade_shape = fade_shape # enum bell, triangle, square, sine, ?
        self.fade_time = fade_time # enum bell, triangle, square, sine, ?
        self.start_time = start_time
        self.star_pixels = [ (0,0,0) ] * (pix_width)
        
    def __init__(self, start_time,display_width):
        self.center = random.randint(0,display_width)
        self.center_start = self.center
        self.center_distance = random.randint(-display_width/2,display_width/2)
        self.hue = random.uniform(0,1) 
        self.saturation = random.uniform(0,1)
        self.brightness = random.randint(128,255)
        self.duration = random.uniform(.5,10)
        self.pix_width = random.randint(10,30)
        self.fade_shape = 0 # enum bell, triangle, square, sine, ?
        self.fade_time = 0 # enum bell, triangle, square, sine, ?
        self.start_time = start_time
        self.star_pixels = [ (0,0,0) ] * (self.pix_width)
        print("new star  self.center:" + str(self.center) + " self.hue:" + str(self.hue) + " self.saturation:" + str(self.saturation) + " self.brightness:" + str(self.brightness) + " self.duration:" + str(self.duration) + " self.pix_width:" + str(self.pix_width) + " self.start_time :" + str(self.start_time) + " ")
    
    def distribute(self,pos,period,shift=0):
        #TODO switch case on fade shape / fade time
        val =  (1) * (math.cos(2*math.pi*(float(pos)/float(period))+math.pi+shift) + 1) / 2
        return val
    
    def calculate_hue(self,pixel):
        delta = time.time() - self.start_time
        peek_time = self.distribute(delta,self.duration)*self.hue
        hue = self.distribute(pixel,self.pix_width)*peek_time
        return hue
        
    def calculate_saturation(self,pixel):
        delta = time.time() - self.start_time
        peek_time = self.distribute(delta,self.duration)*self.saturation
        saturation = self.distribute(pixel,self.pix_width)*peek_time
        return saturation
    
    def calculate_brightness(self,pixel):
        delta = time.time() - self.start_time
        peek_time = self.distribute(delta,self.duration)*self.brightness
        brightness = self.distribute(pixel,self.pix_width)*peek_time
        return brightness
        
    def calculate_center(self):
        delta = time.time() - self.start_time
        self.center = int(self.center_start + ((1)*(self.distribute(delta*.5,self.duration,math.pi)))*self.center_distance/2)
        

    def build_pixels(self):
        now = time.time()
        if self.is_born():
            for pixel in range(self.pix_width):     
                    hue = self.hue # self.calculate_hue(pixel)
                    saturation = self.saturation # self.calculate_saturation(pixel)
                    brightness = self.calculate_brightness(pixel)
                    self.star_pixels[pixel]=colorsys.hsv_to_rgb(hue,saturation,brightness)
            return self.star_pixels
        elif self.is_expired():
            return [ (0,0,0) ] * (self.pix_width)
        else:
            return [ (0,0,0) ] * (self.pix_width)
            
    
    def pixel_blend(self,c1,c2):
        bc=[(0)]*3
        bc[0]=int(math.sqrt(c1[0]*c1[0]/2+c2[0]*c2[0]/2))
        bc[1]=int(math.sqrt(c1[1]*c1[1]/2+c2[1]*c2[1]/2))
        bc[2]=int(math.sqrt(c1[2]*c1[2]/2+c2[2]*c2[2]/2))
        return (bc[0],bc[1],bc[2])
        
    def pixel_max(self,c1,c2):
        bc=[(0)]*3
        bc[0]=int(min(c1[0]+c2[0], 255))
        bc[1]=int(min(c1[1]+c2[1], 255))
        bc[2]=int(min(c1[2]+c2[2], 255))
        return (bc[0],bc[1],bc[2])   
    
                
    def merge_pixels(self, array_to_merge):
        # shift the star so it's center matches 
        # max the array to merge with the star
        self.calculate_center()
        for pixel in range(self.pix_width):
            mapped_pixel=pixel+self.center-self.pix_width/2
            if mapped_pixel < len(array_to_merge) and mapped_pixel >= 0:
                #array_to_merge[mapped_pixel]=self.pixel_blend(array_to_merge[mapped_pixel],self.star_pixels[pixel])
                array_to_merge[mapped_pixel]=self.pixel_max(array_to_merge[mapped_pixel],self.star_pixels[pixel])
        return array_to_merge

    def build_and_merge_pixels(self,array_to_merge):
        self.build_pixels()
        return self.merge_pixels(array_to_merge)
    
    def is_expired(self):
        now = time.time()
        if now > self.start_time + self.duration:
            return 1
        return 0
    
    def is_born(self):
        now = time.time()
        if now >= self.start_time:
            return 1
        return 0
        
def remap(pixel_array):
    #one string is backwards, opc client is suppose to fix this, but it isn't working.
    temp_pixels = [ (0,0,0) ] * (len(pixel_array))
    flipsize = 44
    for i in range(flipsize):
        temp_pixels[flipsize-i] = pixel_array[i]
    for i in range(flipsize):
        pixel_array[i] = temp_pixels[i]
    return pixel_array
    
        

pixels_base = [ (0,0,0) ] * (sum_of_leds)
now = time.time()
star_list = [ star(now,sum_of_leds) for i in range(numOfStars)]
while True:
    now = time.time()
    # clear the array on every loop for now, but would be better if objects cleaned after them selves.
    pixels_base = [ (0,0,0) ] * (sum_of_leds)
    
    # loop through all the stars and build / merge them with background
    for star_index in range(len(star_list)):
        this_star = star_list[star_index]
        if this_star.is_expired():
            star_list[star_index] = star(now,sum_of_leds) # make a new star
        if this_star.is_born():
            pixels_base=this_star.build_and_merge_pixels(pixels_base)
    
    #remap.
    #need to flip the order of the first 44 pixels
    pixels_base = remap(pixels_base)
    client.put_pixels(pixels_base)