# addressable_leds
a place to share addressable led effects

# Drivers
fadecandy not required but it's a good place to start
https://github.com/scanlime/fadecandy (master branch)
OR
skip fade candy and connect diretly with pi
https://github.com/adafruit/Adafruit_NeoPixel (neo branch)

# leds 
I use (warning 24volts dc)
https://www.amazon.com/ALITOVE-Addressable-Programmable-Waterproof-Raspberry/dp/B07PFMN1HP

